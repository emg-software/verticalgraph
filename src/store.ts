import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		filter: JSON.parse ('{}')
	},
	mutations: {
		setFilter (state, newFilter)
		{
			state.filter = newFilter
			console.log ("state.filter", state.filter)
		}
	},
	actions: {
		setFilter ({ commit }, newFilter)
		{
			commit ('setFilter', {
				filter: newFilter
			})
		}
	},
});
