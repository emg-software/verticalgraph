import axios from 'axios'
import { ComboItem } from '@/classes/ComboItem'
const endpoint = 'http://incorporato9784.cloudapp.net/endpoint.php'

class CustomersService 
{
	getAll ()
	{
		return axios.get <ComboItem[]>(`${endpoint}?list=Customers`)
	}
}

export const customersService = new CustomersService()