import axios from 'axios'
import { ComboItem } from '@/classes/ComboItem'
const endpoint = 'http://incorporato9784.cloudapp.net/endpoint.php'

class DomainsService 
{
	getByCustomer (customer: string)
	{
		return axios.get <ComboItem[]>(`${endpoint}?list=Domains&customer=${customer}`)
	}
}

export const domainsService = new DomainsService()