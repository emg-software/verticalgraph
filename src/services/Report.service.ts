import axios from 'axios'
import { ReportFilter } from '@/classes/ReportFilter'
import { GraphItem } from '@/classes/GraphItem'
const endpoint = 'http://incorporato9784.cloudapp.net/endpoint.php'

class ReportService 
{
	getReport (filter: ReportFilter)
	{
		let converted_dt = filter.reference_dt.getDate() + "/" + (filter.reference_dt.getMonth () + 1) + "/" + filter.reference_dt.getFullYear()
		let url = `${endpoint}?list=VerticalReport&customer=${filter.customer}&reference_dt=${converted_dt}&holder_domain=${filter.domain}&manteinances=${filter.manteinances}`
		console.log (url)
		return axios.get <any[]>(url)
	}

	processData (data: any[])
	{
		let reportPerStatus: { [key: string] : GraphItem[] } = {}
		let offset = 0
		let totals: { [key: string] : number } = {}
		totals ["R"] = totals ["T"] = totals ["D"] = totals ["V"] = totals ["C"] = 0
		data.map ((item) => 
		{
			if (offset == 0) 
			{
				let tempDate = new Date (item ['from_dt'] * 1000)
				offset = (new Date (
					[
						tempDate.getFullYear(), 
						tempDate.getMonth() + 1, 
						tempDate.getDate()
					].join ("-"))).getTime() / 1000
			}
			if (item ['from_dt'] < offset) return
			let elapsed = item ['elapsed'] / 60
			let status = item['equipment_status']
			let from = (item ['from_dt'] - offset) / 60
			if (!reportPerStatus [status]) reportPerStatus [status] = []
			reportPerStatus [status].push ({
				position: from,
				length: elapsed
			})
			totals [status] += elapsed
		})
		let payload = 
		{
			totals: totals,
			reportPerStatus: reportPerStatus
		}
		return payload
	}
}

export const reportService = new ReportService()