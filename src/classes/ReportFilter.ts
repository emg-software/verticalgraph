export class ReportFilter {
	constructor (
		public customer: string,
		public domain: string,
		public reference_dt: Date,
		public manteinances: boolean
	) {}
}