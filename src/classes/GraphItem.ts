export class GraphItem {
	constructor (
		public position: number,
		public length: number,
	) {}
}