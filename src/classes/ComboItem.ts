export class ComboItem {
	constructor (
		public index: number,
		public value: string,
		public text: string
	) {}
}