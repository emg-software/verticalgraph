module.exports = {
	baseUrl: process.env.NODE_ENV === 'production'
	  ? '/backend/public/reports/'
	  : '/'
  }